/*
function equalHeight(group) {
	var tallest = 0;
	group.each(function() {
		var thisHeight = $(this).outerHeight(true);
		if(thisHeight > tallest) {
			tallest = thisHeight-5;
		}
	});
	group.height(tallest);
}

$(document).ready(function() {
	setTimeout(function(){
		equalHeight($(".matchheight"));
	}, 300);
});

$(window).bind("load", function() {
   equalHeight($(".matchheight"));
});
*/

// @@ START Media Box 
// JavaScript Document
$(document).ready(function(){
	// set the timeout to account for IE weirdness - triggers function after half a second
	setTimeout(renderBoxes,500);
});
function renderBoxes(){
	// sets box 1 to opaque (selected) state
	var colorArray=['#EB8C00','#DC9600','#E0301E','#DB536A','#A32020','#602320'];
	var arrayDesig=0;
	$('#button_1').css('opacity','1');
	$('#button_1').css('filter','alpha(opacity=100)');
	$('.gx_maxi_numberbox > ul > li').each(function(){
		$(this).css('background-color',colorArray[arrayDesig]);
		arrayDesig+=1;
		if(arrayDesig>5){
			arrayDesig=0;
		}
	});
	// add in mouseover function which adds a white border...
	$('.gx_maxi_numberbox > ul > li').mouseover(function(){
		$(this).css('cursor','pointer');
	});
	// ... and removes it on mouseout.
	$('.gx_maxi_numberbox > ul > li').mouseout(function(){
		$(this).css('cursor','auto');
	});
	// the onclick function for the number boxes triggers the moveTiles() function - see below
	$('.gx_maxi_numberbox > ul > li').click(function(){
		moveTiles(this);
	 });
	$('.gx_maxi_boxwrapper > ul > li').mouseover(function() {												  
		/*$(this).find('.gx_maxi_timedef').css('display','block');*/
		$(this).find('.gx_maxi_timedef').fadeTo(200,0.75);
		$(this).css('cursor','pointer');
	});
	$('.gx_maxi_boxwrapper > ul > li').mouseout(function() {
		$(this).find('.gx_maxi_timedef').css('display','none');												 
		$(this).css('cursor','auto');
	});
}
function moveTiles(_item){
	// first set the opacity for selected / non-selected number boxes
	$('.gx_maxi_numberbox > ul > li').css('opacity','0.75');
	$('.gx_maxi_numberbox > ul > li').css('filter','alpha(opacity=75)');
	$(_item).css('opacity','1');
	$(_item).css('filter','alpha(opacity=100)');
	// take the number from the id name of the clicked number box.
	var targetTile=_item.id.split("_")[1];
	// currentPos is the x position of the div which contains all the tiles.
	var currentPos=$('#boxwrapper').position().left;
	// targetPos is the x position of the tile we want to show.
	var targetPos=$('#tile_'+targetTile).position().left;
	// offset is the distance we need to move the container div...
	var offset=0-targetPos;
	// ...and then we animate it.
	$('#boxwrapper').animate({left: offset}, 500, function() {
  });
}

// @@ END Media Box 

// @@ START Homepage feature 2

// Apple detection object
function checkForApple(){
	var Apple = {};  
	Apple.UA = navigator.userAgent;  
	Apple.Device = false;  
	Apple.Types = ["iPhone", "iPod", "iPad"];  
	for (var d = 0; d < Apple.Types.length; d++) {  
		var t = Apple.Types[d];  
		Apple[t] = !!Apple.UA.match(new RegExp(t, "i"));  
		Apple.Device = Apple.Device || Apple[t];  
	}  
	// is this an Apple device?  
	/*alert(  
		"Apple device? " + Apple.Device +  
		"\niPhone? " + Apple.iPhone +  
		"\niPod? " + Apple.iPod +  
		"\niPad? " + Apple.iPad  
	); */
	if(Apple.iPhone==true || Apple.iPad==true || Apple.iPod==true){
		return true;
	} else {
		return false;
	}
}

$(document).ready(function(){
	setTimeout(init,1500);
});
function init(){
	startAnimation();
}
function startAnimation(){
	setTimeout(startRose,500);
	setTimeout(startTang,1000);
	setTimeout(startYellow,1250);
	setTimeout(startOrange,1500);
	setTimeout(startRed,1750);
	setTimeout(revealText,3000);
}
// animation first stage
function nextYellow(){
	$('#yellow').animate({width:"680px",height:"288px"},1000);
}
function nextTang(){
	$('#tangerine').animate({width:"505px",height:"406px"},750);
}
function nextOrange(){
	$('#orange').animate({width:"608px",height:"288px"},500);
}
function nextRed(){
	$('#red').animate({width:"151px",height:"384px"},800);
}
function nextRose(){
	$('#rose').animate({width:"820px",height:"122px"},1400);
}
// animation second stage
function startYellow(){
	$('#yellow').animate({width:"668px",height:"385px"},1000,nextYellow());
}
function startTang(){
	$('#tangerine').animate({width:"748px",height:"288px"},750,nextTang());
}
function startOrange(){
	$('#orange').animate({width:"442px"},500,nextOrange());
}
function startRed(){
	$('#red').animate({width:"638px"},800,nextRed());
}
function startRose(){
	$('#rose').animate({width:"778px"},1400,nextRose());
}
function revealText(){
	/*alert('text box triggered');*/
	$('#textbox').fadeIn(1000);
}

// @@ END Homepage feature 2
