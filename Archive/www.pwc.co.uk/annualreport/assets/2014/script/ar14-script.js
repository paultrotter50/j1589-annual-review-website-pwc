$(document).ready(function() {

$( "#transcript" ).click(function() {
		$( "#transcript_content" ).slideToggle( "slow" );
		
		if($("#arrow").hasClass("arrow-right")){
			$("#arrow").removeClass("arrow-right");
			$('#arrow').addClass("arrow-bottom");
		}
		else if ($('#arrow').hasClass("arrow-bottom")){
			$('#arrow').addClass("arrow-right");
			$('#arrow').removeClass("arrow-bottom");		
		}		
});

$("a.nu-shareprint").on("click",function(){
 	
 		if (window.print) {
    	window.print()
	} else {
	    alert('Sorry this option is not available on your web browser');
	} 
    
 }); /* end a.nu-shareprint */
 



}); /* end document ready */