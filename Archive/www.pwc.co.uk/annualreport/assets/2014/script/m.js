
$(document).ready(function(){

	/* 
	For Annual Report 2014 only
	stripped down version of full PwC mobile m.js script	
	*/
	
	$('#mobileheadermenu').toggle( 
	   function() { $('#wrapper').animate({ right: 200 }, 'fast', function() { }); },  function() { $('#wrapper').animate({ right: 0 }, 'fast', function() { }); }
	);
	$('#mobileheadersearch').toggle( 
	   function() {
		$('#searchfield').toggle();
		$('#searchfield').val("");
		$('#searchfield').focus();
	   }, 
	function() { $('#searchfield').toggle(); }
	);
	$('#searchfield').keyup(function() {
		if ($('#searchfield').val().length > 0) {
			$('#mobileheadersearch').hide();
			$('#mobilesearchsubmit').show();
		} else {
			$('#mobileheadersearch').show();
			$('#mobilesearchsubmit').hide();
		}
	});

	$('#mobilecopyright > div').jTruncate({  
		length: 50,  
	        minTrail: 0,  
	        moreText: "...more",  
	        lessText: ""
    });  
	
	
	$("#morecopyright").click(function() {
		$("#mobilecopyright > span").show();
		$(this).hide();
		return false;
	});
	

});
