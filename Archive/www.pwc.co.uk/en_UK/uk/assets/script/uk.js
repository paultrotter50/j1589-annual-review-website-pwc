/* ## UK Specific onload ## */
$(document).ready(function(){


/* Test to resize mobile video's for Kevin
	if (jsdevice == "mobile") {
		if ($("#video")) {
			var originalsrc = $("#video").attr('src');
			$("#video").removeAttr('src');
			var iframesrc = originalsrc.replace("1020","320");
			iframesrc = iframesrc.replace("650","204");
			$("#video").attr('src', iframesrc);
			$("#video").css({"width": 320, "height": 204});
			$("iframe").attr("src", $("iframe").attr("src"));
	
		}
	}
*/

	// Remove para spaces
	//document.getElementById("center").innerHTML.replace(/(\<p\>\&nbsp\;\<\/p\>)+/g,'');
	//document.getElementById("center").innerHTML = document.getElementById("center").innerHTML.replace(/(\<p\>\&#160\;\<\/p\>)+/g,'');
	/*
	$(function() { 
		if (!document.getElementById) {
			return false;
		} else {
			document.getElementById("center").innerHTML = document.getElementById("center").innerHTML.replace(/(\<P\>\&nbsp\;\<\/P\>)+/g,'');
			document.getElementById("center").innerHTML = document.getElementById("center").innerHTML.replace(/(\<p\>\&nbsp\;\<\/p\>)+/g,'');
		}
		addscript();
	}); 
	*/
/*
	$('p')
	.filter(function() {
		return $.trim($(this).text()) === '';
	})
	.remove()

*/
$('p').contents().filter(function() { return $.trim($(this).text()) === '' && this.nodeType === 3; }).remove();

	// Fix WIP links
	// Do not publish this to active	
	$(function() { 
		if (location.hostname.indexOf('prd-')>0){
			if (!document.getElementById) {
				return false;
			} else {
				//var links = $("#center").getElementsByTagName('img'); MG JS error fix
				var links = document.getElementsByTagName('img');
				for (var i=0;i<links.length;i++) {
					x = links[i].src.replace(location.protocol+'//'+location.hostname,'');
					if(x.substring(0,1)=='/'&&x.substring(3,4)!='/'&&x.substring(3,4)!='_'){links[i].src = '/en_uk/uk'+x;}

				}
				links = document.getElementsByTagName('a');
				for (var i=0;i<links.length;i++) {
					x = links[i].href.replace(location.protocol+'//'+location.hostname,'');
					//if(x.substring(0,4)=='/en/'){links[i].href = x.substring(4,x.length-4);}
					if(x.substring(0,1)=='/'&&x.substring(3,4)!='/'&&x.substring(3,4)!='_'){links[i].href = '/uk/en'+x;}
				}
			}
		}
	}); 
	// Do not publish this to active
	
});
	
/* ## LEGACY FROM partial common.js ## */

function getCookie(name) {
  var cookieValue = '';
  var prefix = name + "=";

  if (document.cookie.length > 0){
    var offset = document.cookie.indexOf(prefix);

    if (offset != -1){
      offset += prefix.length;
      end = document.cookie.indexOf(";", offset);
      if (end == -1){
        end = document.cookie.length;
      }
      cookieValue = unescape(document.cookie.substring(offset, end));
    }
  }
  return cookieValue;
}

function setCookie(name, value, expires, path, domain, secure) {
  document.cookie = name + "=" + escape(value) +
    ((expires) ? ";expires=" + expires.toGMTString() : "") +
    ((path)    ? ";path=" + path : "") +
    ((domain)  ? ";domain=" + domain : "") +
    ((secure)  ? ";secure" : "");
}

function deleteCookie(name, path, domain) {
  if (getCookie(name)){
    document.cookie = name + "=" +
      ((path) ? "; path=" + path : "") +
      ((domain) ? "; domain=" + domain : "") +
      "; expires=Thu, 01-Jan-1970 00:00:01 GMT";
  }
}

function fixDate(date) {
  var base = new Date(0);
  var skew = base.getTime();

  if (skew > 0){
    date.setTime(date.getTime() - skew);
  }
}

/* Open a URL in a new window if window with same name already exists */
/* the window content is refreshed. If this function is called with */
/* a name in the format  _[anything] the popup window will behave as */
/* normal popup window. */
var g_openWindowList = new Array(); /* global constant for openWindow */

function openWindow(url, name, args) {
	if (name.indexOf("_") != 0) {
		if (typeof(g_openWindowList[name]) != "object") {
			g_openWindowList[name] = window.open(url,name, args);
		} else {
			if (!g_openWindowList[name].closed) {
				g_openWindowList[name].location.href = url;
			} else {
				g_openWindowList[name] = window.open(url, name, args);
			}
		}
		g_openWindowList[name].focus();
		return g_openWindowList[name];
	} else {
		window.open(url, name, args);
	}
	return false;
}

// Open links in new windows - Gets around XHTML 1.0 Strict validators
// Added by John Marshall 
function externalLinks() { 
 if (!document.getElementsByTagName) return; 
 var anchors = document.getElementsByTagName("a"); 
 for (var i=0; i<anchors.length; i++) { 
   var anchor = anchors[i]; 
   if (anchor.getAttribute("href") && 
       anchor.getAttribute("rel") == "external") 
     anchor.target = "_blank"; 
 } 
} 

// Google Analytics event tracking (downloads, mailto and offsite links). D.Montana 05-MAR-2013
$(function(){
        var filetypes = /\.(zip|epub|txt|csv|wmv|ibooks|pdf|doc*|xls*|ppt*|mp3|mp4|f4v|flv|mov)$/i;
        var baseHref = '';
        if ($('base').attr('href') != undefined)
            baseHref = $('base').attr('href');
        $('a').each(function() {
            var href = $(this).attr('href');
            if (href && (href.match(/^https?\:/i)) && (!href.match(document.domain))) {
                $(this).click(function() {
                    var extLink = href.replace(/^https?\:\/\//i, '');
                    _gaq.push(['_trackEvent', 'Offsite', 'Click', extLink]);
                    if ($(this).attr('target') != undefined && $(this).attr('target').toLowerCase() != '_blank') {
                        setTimeout(function() { location.href = href; }, 200);
                        return false;
                    }
                });
            }
            else if (href && href.match(/^mailto\:/i)) {
                $(this).click(function() {
                    var mailLink = href.replace(/^mailto\:/i, '');
                    _gaq.push(['_trackEvent', 'Email', 'Click', mailLink]);
                });
            }
            else if (href && href.match(filetypes)) {
                $(this).click(function() {
                    var extension = (/[.]/.exec(href)) ? /[^.]+$/.exec(href) : undefined;
                    var filePath = href;
                    _gaq.push(['_trackEvent', 'Download', 'Click-' + extension, filePath]);
                    if ($(this).attr('target') != undefined && $(this).attr('target').toLowerCase() != '_blank') {
                        setTimeout(function() { location.href = baseHref + href; }, 200);
                        return false;
                    }
                });
            }
        });
});

// Google Analytics event tracking (modals and share icons). D.Montana 05-MAR-2013
/*
$(function () {
		$("a.shareitem img").click(function () {
			var alttext = $(this).attr("alt");
			_gaq.push(['_trackEvent', 'Share', 'Click-' + alttext, location.pathname]);
		});
		$(".media-overlay").click(function () {
			var src = "";
			if ($(this).attr("media-src")) {
				src = $(this).attr("media-src");
				var fileExtension = "";
				fileExtension = src.substring(src.lastIndexOf(".") + 1, src.length).toLowerCase();
				if (src.search('youtube') >= 0) {
					mediaType = "youtube";
					src = $(src).attr("src");
				} else if ((src.charAt(0) == "#") || (fileExtension == "jhtml") || (fileExtension == "html") || (fileExtension == "htm")) {
					mediaType = "html";
				} else if ((fileExtension == "jpeg") || (fileExtension == "jpg") || (fileExtension == "gif") || (fileExtension == "png")) {
					mediaType = "image";
				} else if (fileExtension == "mp3") {
					mediaType = "audio";
				} else if (fileExtension == "swf") {
					mediaType = "flash";
				} else {
					mediaType = "akamaivideo";
				}
				_gaq.push(['_trackEvent', 'Modal', 'Click-' + mediaType, src]);
			}
		});
});	
*/
// Google Analytics video tracking for the Akamai-based media player. D.Montana 05-MAR-2013
function sendToHtml(val4, val2, val1){ 
	var ga4 = val4; 
	var ga2 = val2;
	var ga1 = val1; 
	_gaq.push(['_trackEvent', 'Media', ga4, ga2]); 
}
