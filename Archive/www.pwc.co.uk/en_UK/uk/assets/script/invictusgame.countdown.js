// JavaScript Document

$( document ).ready(function() {
            var endDate = new Date("2014-09-10T19:00:00.000+01:00");            
var dateStr = endDate.getFullYear().toString()+'/'+(endDate.getMonth() +1 ).toString()+'/'+endDate.getDate().toString()+ ' ' + endDate.getHours().toString() + ':' + 
endDate.getMinutes().toString() + ':' + endDate.getSeconds().toString();
$('#clock').countdown(dateStr, function(event) {
                var $this = $(this).html(event.strftime(''
                + '<div class="info"><span>%-D</span> Day%!d </div>'
                + '<div class="info"><span>%H</span> Hour%!H</div>'
                + '<div class="info"><span>%M</span> Minute%!M</div>'
                + '<div class="info last"><span>%S</span> Second%!S</div><br clear="all" />'));
            });
        });