<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-uk" lang="en-uk">
<head>
        <!-- Page Metadata -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="en-uk" />
	<meta http-equiv="Last-Modified" content="Tue Jul 08 05:40:28 EDT 2014" />

	
    <meta property="og:title" content="Living standards"/>

    <meta property="og:type" content="article"/>

	<meta property="og:url" content="http://www.pwc.co.uk/government-public-sector/2015-and-beyond/living-standards/index.jhtml"/>
    <meta property="og:site_name" content="PwC"/>
	<meta property="og:description" content="Living standards"/>
        <meta http-equiv="cache-control" content="no-cache" />
        <meta name="robots" content="index,noarchive" />
        <meta name="description" content="Living standards" />
	<meta name="author" content="PricewaterhouseCoopers" />

		<meta name="lifecyclevm" content="active" />
	<meta name="statevm" content="www.pwc.co.uk/" />
	
	
	
	
	<meta name="lifecycleesi" content="active" />
	<meta name="localeesi" content="en_UK" />
	<meta name="countryesi" content="uk" />
	
		
			
		
	<meta name="clientvm" content="Wget/1.10.2" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../../../../../www.pwc.com/en_US/gx/assets/image/apple-touch-icon-144x144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../../../../../www.pwc.com/en_US/gx/assets/image/apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../../../../../www.pwc.com/en_US/gx/assets/image/apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="../../../../../../www.pwc.com/en_US/gx/assets/image/apple-touch-icon-precomposed.png">
	
    <!-- Page Title -->
	<title>Living standards</title>

	<!-- Meta tags for CBS -->
        <meta name="pwcTitle" content="Living standards" />
        <meta name="pwcReleaseDate" content="Thu Apr 10 00:00:00 EDT 2014" />
        <meta name="pwcCountry" content="uk" />
        <meta name="pwcGeo.default" content="uk" />
	<meta name="pwcLocale" content="en_UK" />
        <meta name="pwcLang" content="en" />
																																																																																																																						
			<meta name="pwcService" content="CONS" />
		        <meta name="pwcDB" content="services" />
        <meta name="pwcDBDate1" content="" />
        <meta name="pwcDBDate2" content="" />
        <meta name="pwcContact" content="0" />
        <meta name="pwcGeo" content="United Kingdom " />
        <meta name="pwcHideLevel" content="0,2" />
        <meta name="pwcViewAgent" content="1" />

	<!-- Meta tag for RVP -->
        <meta name="RVPFlag" content="1" id="RVPFlag" />
        <meta name="RVPTitle" content="Living standards" id="RVPTitle" />

	<!--Canonical URL -->
	<link rel="canonical" href="../../../../../government-public-sector/2015-and-beyond/living-standards/index.jhtml" />

        <!-- Stylesheets -->



	
		
			<link rel="stylesheet" type="text/css" href="../../../../../en_GX/webadmin/assets/style/base.css" media="screen" />
			<link rel="stylesheet" type="text/css" href="../../../../../en_GX/webadmin/assets/style/rose.css" media="screen" />
															<link rel="stylesheet" type="text/css" href="../../../../../en_GX/webadmin/assets/style/print.css" media="print" />
											


	<!-- Fav Icon -->
	<link rel="icon" href="../../../../../en_GX/webadmin/assets/image/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="../../../../../en_GX/webadmin/assets/image/favicon.ico" type="image/x-icon" />
	
	<!-- Javascript -->
	<script type="text/javascript" src="../../../../../en_GX/webadmin/assets/script/jquery.js"></script>
	<script type="text/javascript" src="../../../assets/script/local.js"></script>
	<script type="text/javascript" src="../../../../../en_GX/webadmin/assets/script/scripts.js"></script>
				
		<script type="text/javascript" src="../../../../../en_GX/webadmin/assets/script/pwc-media.js"></script>
	

        <!-- Custom script, css and rss -->

<!-- Custom head tag code -->
        <link href="../../../assets/style/uk.css" media="screen" rel="stylesheet" type="text/css"></link>
<script src="../../../assets/script/uk.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46301177-1', 'pwc.co.uk');
  ga('require', 'linkid', 'linkid.js');
  ga('send', 'pageview');
-->
</script>
<link href="../../../../../en_GX/webadmin/assets/style/micrositelhn.css" media="screen" rel="stylesheet" type="text/css"></link>
<link href="http://www.pwc.co.uk/en_uk/uk/government-public-sector/2015-and-beyond/assets/jquery-countdown.css" media="screen" rel="stylesheet" type="text/css"></link> 
<link href="../../../../../en_GX/webadmin/assets/style/micrositelhn.css" media="screen" rel="stylesheet" type="text/css"></link> 
<link href="../../../assets/style/customslider.css" media="screen" rel="stylesheet" type="text/css"></link>
<link href="../../../_standards/assets/css/gew.css" media="screen" rel="stylesheet" type="text/css"></link>
<script src="../../../../../../www.pwc.com/gx/en/twitter-cache/assets/script/pwc-twitter-widget-1.0.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../../../../../../www.pwc.com/gx/en/twitter-cache/assets/style/pwc-twitter-widget-1.0.css">
<link href="../../../multimedia/style/multimedia.css" media="screen" rel="stylesheet" type="text/css"></link>
<script type="text/javascript">
            $(document).ready(function () {
                
                $('#tweet_box').pwcTwitterWidget({account:'pwc_ukgov', tweetCount:5,  width:'auto', culture: 'en-US', events: {
'afterRender': function(){
$('.matchheight').height('auto');
var highestBox = 0;
$('.matchheight').each(function(){
if($(this).height() > highestBox)
highestBox = $(this).height();
});
$('.matchheight').height(highestBox);
}
}});
            });
</script>

<style> 
#pwcmobile .infographic {display: none;}
h1 {display:none !important;}
#imagetitlecontainer h2 {color: #DB536A; font-size: 2em; font-style: italic; line-height: 1em; margin: 0 0 10px 0;}
#imagetitlecontainer img.icon {float: left; padding-right: 10px;}
#imagetitlecontainer {width: 100%; margin: 0; border-top: 4px solid #DB536A; padding-top: 10px;}
.infographic {float: right; margin: 10px 0 0 10px;}
#pwcslider.fsreghub {
    padding-bottom: 35px;
}
h3 {font-size:1.2em;}
.rhbox li {border-bottom:none;}
.backgroundbox {margin-bottom:15px; background-color:#fbeef0 !important;}
.rhbox {margin-top:15px;}
.vidicon {
    background-image: url("/en_GX/webadmin/assets/image/media-play-button.gif");
    background-position: right bottom;
    background-repeat: no-repeat;
}
#pwcmobile #twitter_container {width:100%; float:none; margin-left:0;}
#twitter_container h3 {font-size:1.2em !important; margin-left:0;}
#pwcmobile #budgetblog {width:100%; float:none;}
#budgetblog {width:248px; float:left;margin-left:10px; width:253px; padding:7px 10px; background-color: #F5F4F0; margin-top:5px;}
#budgetblog h3 {font-size:1em !important; }
#budgetblog h3 a {font-size:1em !important; color:#db536a !important; underline:none !important; margin:0.2em 0;}
#budgetblog p {margin: 0 !important;}
#budgetblog p a {color:db536a !important;}
.mm-pic-left {width:85px; margin-right:5px; height:60px;}
#pwcmobile .mm-pic-left {float:left;}
.mm-content-right {width:168px;}
.mm-content-right p {margin:0 !important;}
#budgetholder {width: 556px; height: 410px; margin: 15px 0 15px 0;}
#pwcmobile #budgetholder {width: 100%; height: 100%;}
#budgetholder a {text-decoration:none !important;}
#budgetholder a:hover {text-decoration:underline !important;}
#budgetholder-left {width: 258px; height:390px; float: left; background-color: #e5e2dd; padding: 10px; color: #602320; margin-right: 1px;}
#pwcmobile #budgetholder-left {width: 1000%; height:100%; float: none;}
#budgetholder-left h3 {margin:0 !important;}
#budgetholder-right {width: 277px; float: left; color: #fff;}
#pwcmobile #budgetholder-right {width: 100%; float: none;}
#budgetholder-right h3 {margin:0; width:62%;}
#budgetholder-right h3 a {color:#fff !important;}
#budgetholder-right p {width:66%; margin:2px 0; }
#budgetholder-right p a {color:#fff !important;}
.budget-right-block {height:82px; padding:10px;}
#pwcmobile .budget-right-block {width:100%;}

#twitter_container p {
    padding: 0 10px;
	margin:0;
	-webkit-margin-before: 0;
	-webkit-margin-after: 0;
	-webkit-margin-start: 0;
	-webkit-margin-end: 0;
	padding-bottom:3px;
}

#twitter_container p a, #tweet_box .screen-name a {
    color: #DB536A !important;
    text-decoration: none !important;
    font-weight:bold;
}
#twitter_container {
width: 268px;
margin-left: 0px;
float: left;
background-color: #F5F4F0;
}
#pulsepoll.Burgundy h2 {color:#fff !important;}

#gew .fivePanels_twoFeature .B {
    height: 120px;
    margin: 0 1px 1px 0;
    padding: 10px;
    position: relative;
    width: 256px;
}
#gew .fivePanels_twoFeature .B2 {
    height: 119px;
    margin: 0 1px 1px 0;
    padding: 10px;
    position: relative;
    width: 256px;
}
.vidicon {
    background-image: url("/en_GX/webadmin/assets/image/media-play-button.gif");
    background-position: left bottom;
    background-repeat: no-repeat;
}
#pulsepoll.Burgundy {color:#ffffff !important;}
</style>
<script type="text/javascript" src="../../../../../en_uk/uk/budget/assets/swfobject.js"></script>
<script type="text/javascript">
            $(document).ready(function () {
                
                $('#tweet_box').pwcTwitterWidget({account:'pwc_ukgov', tweetCount:5,  width:'auto', culture: 'en-US', events: {
'afterRender': function(){
$('.matchheight').height('auto');
var highestBox = 0;
$('.matchheight').each(function(){
if($(this).height() > highestBox)
highestBox = $(this).height();
});
$('.matchheight').height(highestBox);
}
}});
            });
</script>
<script type="text/javascript" src="http://www.pwc.co.uk/en_uk/uk/government-public-sector/2015-and-beyond/assets/jquery-countdown-min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
	$('#holder').countdown({until: new Date(2015, 5-1, 7, 19, 12, 30, 0),format:'dHMS'});
})

</script>


</head>


<body id="pwcdesktop" onload="addscript();">

	

      <div id="wrapper"><!-- START Header --><layout_html xmlns:esi="http://www.edge-delivery.org/esi/1.0">
<div id="header"><!-- START Header --><!-- Logo, country & languages -->
<div id="headerleft"><div style="display: none;">Tuesday July 08, 2014</div>
<div id="headerlogo">
	<a href="../../../../../index.html">
		<img src="../../../../../en_GX/webadmin/assets/image/pwclogo.gif" alt="PwC logo" border="0" width="94" height="72" />
	</a>
</div></div>


<!-- Search -->
<div id="headerright">

	
	
		
	<br/>
	 <div id="searchbox">
		<form action="http://www.pwc.co.uk/en_GX/webadmin/search/search.jhtml" id="frmsearch" method="get" name="frmsearch">
		<div id="headercountry"><span>United Kingdom</span></div> 
		<label for="searchfield"><img alt="Search" border="0" height="1" src="../../../../../en_GX/webadmin/assets/image/tran.gif" width="1"/></label>
		<input accesskey="4" id="searchfield" name="searchfield" onfocus="clearsearch(); return false;" onkeyup="showResult(this.value);" size="30" type="text" value=""/><input alt="Search" id="searchsubmit" name="searchsubmit" src="../../../../../en_GX/webadmin/assets/image/searchsubmit.gif" type="image"/><br/>
		<a href="index.jhtml#" onclick="clearlivesearch(); return false;" title=""><img alt="" border="0" id="livesearchbutton" src="../../../../../en_GX/webadmin/assets/image/tran.gif"/></a>
		<div id="livesearch"><!--x--></div>
		<input name="pwcGeo" type="hidden" value="UK"/>
		<input name="pwcLang" type="hidden" value="en"/>
		<input name="pwcHideLevel" type="hidden" value="0"/>
		<input name="localeOverride" type="hidden" value="en_UK"/>
		</form>
	</div><br/>
	<div id="headerlang">
		<a accesskey="3" href="../../../../../../www.pwc.com/gx/en/site-index.jhtml" id="intllink">International PwC Sites</a>
	</div>   
	 <div id="popupclose">Click the 'X' in the top right corner to close this window.</div>
		

</div>

	
		
	<!-- Primary navigation --><noindex>
<div id="navblock">
    <ul id="nav">
	<li class="navsingle nav0"><a  href="../../../../../index.html"   class="navstaticlink">Home</a>
	</li>
	<li id="navselected"  class="nav1"><a  href="../../../../../industries/index.jhtml"  >Industries</a>
		<ul class="navcol1">
			<li><a  href="../../../../../aerospace-defence/index.jhtml"   >Aerospace, defence & security</a>
	</li>
			<li><a  href="../../../../../asset-management/index.jhtml"   >Asset management</a>
	</li>
			<li><a  href="../../../../../automotive/index.jhtml"   >Automotive</a>
	</li>
			<li><a  href="../../../../../banking-capital-markets/index.jhtml"   >Banking & capital markets</a>
	</li>
			<li><a  href="../../../../../business-services/index.jhtml"   >Business services</a>
	</li>
			<li><a  href="../../../../../capital-projects-infrastructure/index.jhtml"   >Capital projects and infrastructure</a>
	</li>
			<li><a  href="../../../../../charities/index.jhtml"   >Charities</a>
	</li>
			<li><a  href="../../../../../chemicals/index.jhtml"   >Chemicals</a>
	</li>
			<li><a  href="../../../../../communications/index.jhtml"   >Communications</a>
	</li>
			<li><a  href="../../../../../engineering-construction/index.jhtml"   >Engineering & construction</a>
	</li>
			<li><a  href="../../../../../entertainment-media/index.jhtml"   >Entertainment & media</a>
	</li>
			<li><a  href="../../../../../financial-services/index.jhtml"   >Financial services</a>
	</li>
			<li><a  href="../../../../../forest-paper-packaging/index.jhtml"   >Forest, paper & packaging</a>
	</li>
			<li><a  href="../../../../../government-public-sector/index.jhtml"   >Government & public sector</a>
	</li>
</ul>
<ul class="navcol2">
			<li><a  href="../../../../../hospitality-leisure/index.jhtml"   >Hospitality & leisure</a>
	</li>
			<li><a  href="../../../../../insurance/index.jhtml"   >Insurance</a>
	</li>
			<li><a  href="../../../../../manufacturing/index.jhtml"   >Manufacturing</a>
	</li>
			<li><a  href="../../../../../metals/index.jhtml"   >Metals</a>
	</li>
			<li><a  href="../../../../../mining/index.jhtml"   >Mining</a>
	</li>
			<li><a  href="../../../../../oil-gas/index.jhtml"   >Oil & gas</a>
	</li>
			<li><a  href="../../../../../pharmaceuticals-life-sciences/index.jhtml"   >Pharmaceuticals & life sciences</a>
	</li>
			<li><a  href="../../../../../power-utilities/index.jhtml"   >Power & utilities</a>
	</li>
			<li><a  href="../../../../../private-equity/index.jhtml"   >Private equity</a>
	</li>
			<li><a  href="../../../../../real-estate/index.jhtml"   >Real estate</a>
	</li>
			<li><a  href="../../../../../retail-consumer/index.jhtml"   >Retail & consumer</a>
	</li>
			<li><a  href="../../../../../technology/index.jhtml"   >Technology</a>
	</li>
			<li><a  href="../../../../../transport-logistics/index.jhtml"   >Transport & logistics</a>
			</li>
		</ul>
        </li>
	<li class="nav2"><a  href="../../../../../issues/index.jhtml"  >Issues</a>
		<ul class="navcol1">
			<li><a  href="../../../../../economic-services/the-economy.jhtml"   >The economy</a>
	</li>
			<li><a  href="../../../../../issues/issues-governance.jhtml"   >Governance</a>
	</li>
			<li><a  href="../../../../../issues/issues-innovation-and-technology.jhtml"   >Innovation & technology</a>
	</li>
			<li><a  href="http://www.pwc.co.uk/en_UK/uk/government-public-sector/2015-and-beyond/living-standards/en/uk/issues/megatrends/index.jhtml"   >Megatrends</a>
	</li>
			<li><a  href="../../../../../issues/issues-operations.jhtml"   >Operations</a>
	</li>
</ul>
<ul class="navcol2">
			<li><a  href="../../../../../issues/regulation.jhtml"   >Regulation</a>
	</li>
			<li><a  href="../../../../../issues/issues-risk.jhtml"   >Risk</a>
	</li>
			<li><a  href="../../../../../issues/strategy-and-growth.jhtml"   >Strategy & growth</a>
	</li>
			<li><a  href="../../../../../issues/issues-talent.jhtml"   >Talent</a>
			</li>
		</ul>
        </li>
	<li class="nav3"><a  href="../../../../../services/index.jhtml"  >Services</a>
		<ul class="navcol1">
			<li><a  href="../../../../../audit-assurance/index.jhtml"   >Audit & assurance</a>
	</li>
			<li><a  href="../../../../../tax/index.jhtml"   >Tax</a>
	</li>
			<li><a  href="../../../../../consulting/index.jhtml"   >Consulting</a>
	</li>
			<li><a  href="../../../../../business-recovery/index.jhtml"   >Business recovery</a>
	</li>
			<li><a  href="../../../../../corporate-finance/index.jhtml"   >Corporate finance</a>
	</li>
			<li><a  href="../../../../../human-resource-services/index.jhtml"   >Human resource services</a>
	</li>
			<li><a  href="../../../../../economics-policy/index.jhtml"   >Economics and policy</a>
	</li>
</ul>
<ul class="navcol2">
			<li><a  href="../../../../../transaction-services/index.jhtml"   >Transaction services</a>
	</li>
			<li><a  href="../../../../../strategy/index.jhtml"   >Strategy</a>
	</li>
			<li><a  href="../../../../../sustainability-climate-change/index.jhtml"   >Sustainability & climate change</a>
	</li>
			<li><a  href="../../../../../forensic-services/index.jhtml"   >Forensic services</a>
	</li>
			<li><a  href="../../../../../actuarial/index.jhtml"   >Actuarial</a>
	</li>
			<li><a  href="http://www.pwclegal.co.uk"   >PwC Legal LLP</a>
			</li>
		</ul>
        </li>
	<li class="navsingle nav4"><a  href="../../../../../publications/index.jhtml"   class="navstaticlink">Publications</a>
	</li>
	<li class="nav5"><a  href="../../../../../who-we-are/index.jhtml"  >Who we are</a>
		<ul class="navcol1">
			<li><a  href="../../../../../annualreport/index.jhtml"   >Annual Report</a>
	</li>
			<li><a  href="../../../../../who-we-are/executive-board.jhtml"   >Leadership</a>
	</li>
			<li><a  href="../../../../../corporate-sustainability/index.jhtml"   >Corporate sustainability</a>
	</li>
			<li><a  href="../../../../../who-we-are/competition-commissions-audit-market-investigation.jhtml"   >Competition Commission</a>
	</li>
			<li><a  href="../../../../../who-we-are/engaging-with-and-advising-policy-makers.jhtml"   >Engaging with policy makers</a>
	</li>
			<li><a  href="../../../../../who-we-are/office-locations.jhtml"   >UK offices</a>
	</li>
</ul>
<ul class="navcol2">
			<li><a  href="http://pwc.blogs.com/regional-websites.html"   >Regional sites</a>
	</li>
			<li><a  href="http://pwc.blogs.com/"   >Blogs</a>
	</li>
			<li><a  href="../../../../../events/index.jhtml"   >Events</a>
	</li>
			<li><a  href="../../../../../who-we-are/pwc-uk-iphone-app-download.jhtml"   >Mobile apps</a>
	</li>
			<li><a  href="../../../../../who-we-are/alumni-programme.jhtml"   >Alumni</a>
	</li>
			<li><a  href="../../../../../who-we-are/consultation-responses.jhtml"   >Consultation responses</a>
			</li>
		</ul>
        </li>
	<li class="navsingle nav6"><a  href="../../../../../careers/index.jhtml"   class="navstaticlink">Careers</a>
	</li>
	<li class="navsingle nav7"><a  href="http://pwc.blogs.com/press_room/"   class="navstaticlink">Media centre</a>
  </li>
    </ul>
</div>
<div id="navbaseline"></div>
</noindex> <!-- END Header -->
		
</div> <!-- END Header --> 

<div id="container"><!-- START page container -->
<div class="column" id="center"><!-- START Content column -->
<h1>Living standards</h1>

 <!-- START Body area --> 
 
<div id="imagetitlecontainer"><img class="icon" src="http://www.pwc.co.uk/en_uk/uk/government-public-sector/2015-and-beyond/assets/images/living-standards-thumb.gif"/> 

<h2>Living standards</h2>

<p><strong>What more must government do to help lift living standards? Will the costs of living continue to rise? How can creating good jobs contribute to this agenda? Find out more.</strong></p>
</div>

<div class="clearer"><!--x--></div>

<div class="fsreghub" id="pwcslider">
<div class="sliderhead">Feature content</div>

<div class="items">
<div class="feature">
<div class="fsregfeature rose" style=" background-image: url(/en_uk/uk/government-public-sector/2015-and-beyond/assets/images/living-standards-hub-infographic.jpg);">
<div class="fsregcontent"><small>Infographic</small><a href="http://www.pwc.co.uk/government-public-sector/2015-and-beyond/living-standards/lifting-living-standards-did-you-know.jhtml">Living standards</a></div>

<div class="fsregsub"><a href="http://www.pwc.co.uk/government-public-sector/2015-and-beyond/living-standards/lifting-living-standards-did-you-know.jhtml">Is the big squeeze nearly over?</a></div>
</div>

<div class="fsregfeature halfheight red bgimage" style=" background-image: url(/en_uk/uk/government-public-sector/2015-and-beyond/assets/images/shopping-trolly-hub.jpg);">
<div class="fsregcontent"><small>Events</small><a href="http://www.pwc.co.uk/government-public-sector/2015-and-beyond/growth/good-jobs.jhtml">Creating good jobs?<span>What we found from our recent roundtables on good jobs and their role in lifting living standards</span></a></div>
</div>

<div class="fsregfeature halfheight orange">
<div class="fsregcontent"><small>Research</small><a href="http://www.pwc.co.uk/pensions/publications/one-size-fits-none.jhtml">Does the flexible workforce of the future need a flexible State Pension Age?<span>Find out more</span></a></div>
</div>
</div>
</div>
</div>

<div class="clearer"><!--x--></div>

<div id="gew">
<hr/>
<div class="contentfeature red halfcontent matchheight" style=" margin-bottom: 13px;">
<h2>@pwc_ukgov on Twitter</h2>

<div id="twitter_container">
<div id="tweet_box"><img alt="" src="../../../../../../www.pwc.com.uy/es/assets/script/twitter/ajax-loader.gif"/></div>

<p style=" clear: both;"><a href="http://twitter.com/pwc_ukgov" target="_blank">More...</a></p>
</div>
</div>

<div class="contentgutter"><!--x--></div>

<div id="budgetblog">

<div id="typepad">
 <h2><a href="http://pwc.blogs.com/publicsectormatters/2015-and-beyond/ ">2015 and beyond blogs<br/>Living standards</a></h2>
</div>
<script>
 <!-- 
(function() {
var domInsert ="";
jQuery.getJSON('http://api.typepad.com/blogs/6a00d83451623c69e20163017ca5fa970d/post-assets/@by-category/Living%20standards.js?max-results=3' + "&callback=?", function(data){ 
   domInsert ='<h2><a href="http://pwc.blogs.com/publicsectormatters/2015-and-beyond/ " target="_blank" style="text-decoration:none !important; color:#A32020 !important;">2015 and beyond blogs - living standards</a></h2>';
   var blurb = "";
   jQuery.each(data.entries, function(){ 
       blurb = this.excerpt.substring(0, 115); blurb = blurb.substring(0, blurb.lastIndexOf(" "));
       domInsert += '<div class="tpEntry"><h3><a href="' + this.permalinkUrl + '" target="_blank">' + this.title + '</a></h3>' + '<p>' + blurb + '... <a href="' + this.permalinkUrl + '" target="_blank"><small>Read more...</small></a></p></div>';
	});
if(domInsert != ''){$('#typepad').html(domInsert)}
})})();
  --> 
  </script>
</div> 

<div class="clearer"><!--x--></div>
</div> <!-- END Body area --></div>

<div class="column" id="left"><!-- START Left column --><!-- Left Hand Navigator -->













































<noindex>
<div class="leftnav" id="lhnrose">
<ul>
<li  class="lhn1"><div class="lhnentry"><a   href="../../../../../government-public-sector/index.jhtml"  >Government & public sector</a></div>
</li>
<li  class="lhn2"><div class="lhnentry"><a   href="../../../../../government-public-sector/central-government/index.jhtml"  >Central government</a></div>
</li>
<li  class="lhn6"><div class="lhnentry"><a   href="../../../../../government-public-sector/issues/higher-apprenticeships.jhtml"  >Education</a></div>
</li>
<li  class="lhn19"><div class="lhnentry"><a   href="../../../../../government-public-sector/healthcare/index.jhtml"  >Healthcare</a></div>
</li>
<li  class="lhn28"><div class="lhnentry"><a   href="../../../../../government-public-sector/local-government/index.jhtml"  >Local government</a></div>
</li>
<li  class="lhn35"><div class="lhnentry"><a   href="../../../../../government-public-sector/international-development/index.jhtml"  >International development</a></div>
</li>
<li  class="lhn41"><div class="lhnentry"><a   href="../../../../../government-public-sector/transport/index.jhtml"  >Transport</a></div>
</li>
<li  class="lhn43"><div class="lhnentry"><a   href="../../../../../government-public-sector/publications/index.jhtml"  >Insights</a></div>
<ul>
<li  class="lhn44"><div class="lhnentry"><a   href="../../../../../government-public-sector/2015-and-beyond/index.jhtml"  >2015 and beyond</a></div>
<ul>
<li id="selected" class="lhn45"><div class="lhnentry"><a   href="../../../../../government-public-sector/2015-and-beyond/living-standards/index.jhtml"  >Living standards</a></div>
</li>
<li  class="lhn46"><div class="lhnentry"><a   href="../../../../../government-public-sector/2015-and-beyond/growth/index.jhtml"  >Growth</a></div>
</li>
<li  class="lhn47"><div class="lhnentry"><a   href="../../../../../government-public-sector/2015-and-beyond/reform/index.jhtml"  >Reform</a></div>
</li>
<li  class="lhn48"><div class="lhnentry"><a   href="../../../../../government-public-sector/2015-and-beyond/our-insights.jhtml"  >Our insights</a></div>
</li>
</ul>
</li>
<li  class="lhn49"><div class="lhnentry"><a   href="../../../../../government-public-sector/ceo-survey/index.jhtml"  >CEO Survey</a></div>
</li>
<li id="selected" class="lhn50"><div class="lhnentry"><a   href="index.jhtml"  >Stepping Stones</a></div>
</li>
<li  class="lhn52"><div class="lhnentry"><a   href="../../../../../government-public-sector/good-growth/index.jhtml"  >Good growth</a></div>
</li>
<li  class="lhn53"><div class="lhnentry"><a   href="../../../../../government-public-sector/education/the-new-sorp-and-frs-102-for-higher-education-and-universities-a-guide.jhtml"  >The education SORP and FRS 102</a></div>
</li>
</ul>
</li>
<li  class="lhn54"><div class="lhnentry"><a   href="../../../../../government-public-sector/gps-contacts.jhtml"  >Contacts</a></div>
</li>
</ul>
</div>
</noindex><!-- Webtiles --><br/> <!-- END Left column --></div>

<div class="column" id="right"><!-- START Right column -->
		<!-- START JS Send & Share icons & links -->
<script type="text/javascript">
var sharetitle = "Living standards";

function emailpage(){
var urlToOpen = "/en_GX/webadmin/forms/email_a_colleague.jhtml?localeOverride=en_UK&color_stylesheet=rose&urlref=Kf9lIRgK2tgbaab7B6XNC1Vc0yZwAGZQUV7xwJBQ0Plwkn2RIoxTYzeXK7Va8B/WQVE/o3yFQt9OmA7PPoBQ+5DPVDoqWzdezOfLKGmMnhReNxiRMNv+4kin5L7w6kAg";
PopUp = window.open( urlToOpen, 'Popup', 'width=450,height=500,menubar=no,scrollbars=yes,resizable=yes' );
}

function writesharelinks() {
	var sharelinks = ""
	for (i=0;i<arr_ss.length; i++) {
		var preurl = unescape(arr_ss[i][1]);
		var posturl = arr_ss[i][2];
		if (window.location.href.indexOf("?") !=-1 ) {
			posturl = posturl.replace("?","&");
		}
		while(preurl.indexOf('+')!=-1) { preurl = preurl.replace('+','&2b'); }
		if (window.location.href.indexOf("?display=") !=-1 ) {
			posturl = posturl.replace("?","&");
			if (preurl.indexOf("facebook.com") !=-1 ) {
				preurl = preurl.replace("?display=","?nodisplay=");
			}
		}
		sharelinks = sharelinks + "<li id='share" + i + "'><a href='http://www.pwc.co.uk/en_UK/uk/government-public-sector/2015-and-beyond/living-standards/%22%20+%20preurl%20+%20posturl%20+%20%22' onclick='" + arr_ss[i][4] + "' target='" + arr_ss[i][5] + "' class='shareitem' id='shareitem" + i + "'><img src='http://www.pwc.co.uk/en_UK/uk/government-public-sector/2015-and-beyond/living-standards/%22%20+%20arr_ss[i][0]%20+%20%22' border='0' alt='" + arr_ss[i][3] + "' /></a></li>";	
	}
	document.write(sharelinks);
}

var newwindow;
function popup(url)
{
	newwindow=window.open(url,'_blank');
	if (window.focus) {newwindow.focus()}
}

</script>

<!-- Send & share box -->
<div id="pagetoolbox">
     		<ul id="pagetoolsnew">
     			<li class="shareprint"><a href="index.jhtml#" class="shareitem"><img src="../../../../../en_GX/webadmin/assets/image/share_print.gif" alt="Print-friendly version" /></a></li>
     			<script type="text/javascript">writesharelinks();</script>
     		</ul>
			<div class="clearer"> </div>
</div>
		<div id="ukpagetoolbox">
			<ul id="pagetools">
				<li id="ptemail"><a class="shareicon" href="javascript:emailpage();">Email to a colleague</a></li>
				<li id="ptprint"><a class="shareicon" href="javascript:popup(window.location+'?preview=true');">Print-friendly version</a></li>
			</ul>
		</div>
		<div id="uksociallinks">
			<div class="uk-twitter"><a class="twitter-share-button" data-count="horizontal" data-via="PwC_UK" href="http://twitter.com/share">Tweet</a><script src="http://platform.twitter.com/widgets.js" type="text/javascript">//comment</script></div>
			<div class="uk-linkedin"><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.pwc.co.uk/government-public-sector/2015-and-beyond/living-standards/index.jhtml?id=1404812428868&amp;title=Living standards" target="new"><img alt="Share with LinkedIn" src="../../../../../assets/images/linkedin.jpg"/></a></div>
			<div class="uk-facebook">
				<div id="fb-root"><!-- for IE --></div>
				<script>
					(function(d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) {return;}
						js = d.createElement(s); js.id = id;
						js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
						fjs.parentNode.insertBefore(js, fjs);
					}
					(document, 'script', 'facebook-jssdk'));
				</script>
				<div class="fb-like" data-font="arial" data-layout="button_count" data-send="false" data-show-faces="true" data-width="200"> </div>
			</div>
			<div class="uk-gplus">
				<div class="g-plusone" data-size="medium"><!-- for IE --></div>
				<script type="text/javascript">
					window.___gcfg = {lang: 'en-GB'};
					(function() {
						var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
						po.src = 'https://apis.google.com/js/plusone.js';
						var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
					})();
				</script>
			</div>
		</div>
		
			<div class="webtile">
	   <div class="webtile"><a class="wbtile bgbase" href="../../../../../the-economy/publications/uk-economic-outlook/living-standards-is-the-big-squeeze-nearly-over-ukeo-march14.jhtml" target="_blank"><span class="wbtilehead">Insight from our March UK Economic Outlook?</span>Find out more</a></div>
	</div>
	<div class="webtile">
	   <div class="countdownbox" id="countdown">
<h3>Countdown to  General election: <br/>7th May 2015</h3>
<div class="countdownholder" id="holder"><!--x--></div>
</div>
	</div>
	<div class="webtile">
	   <a class="wbtile bgbase pagelevelwebtile" href="http://pwc.blogs.com/publicsectormatters/email-alerts.html" target="_blank"><span class="wbtilehead">Public sector matters email alerts</span> <span class="wbtiledesc">Join today to receive email alerts when we publish new posts</span></a>
	</div>
 <!-- END Right column --></div>
</div>

<layout_html xmlns:esi="http://www.edge-delivery.org/esi/1.0">
	<!-- Footer navigation band --> 
	
		
		<div id="footer-wrapper">
			<div id="footer">    <ul id="footernav">





        <li class="nav1"><a     href="../../../../../who-we-are/office-locations.jhtml">Offices</a></li>




        <li class="nav2"><a     href="../../../../../sitemap.jhtml">Sitemap</a></li>




        <li class="nav3"><a     href="../../../../../who-we-are/alumni-programme.jhtml">Alumni</a></li>



















</ul></div>
			<div id="footerlinks">&#169;&#160;2012-2014 PwC. All rights reserved. PwC refers to the PwC network and/or one or more of its member firms, each of which is a separate legal entity. Please see <a href="http://www.pwc.com/structure">www.pwc.com/structure</a> for further details.    <ul>











        <li class="nav4"><a     href="../../../../../who-we-are/privacy-statement.jhtml">Privacy Statement</a></li>




        <li class="nav5"><a     href="../../../../../who-we-are/cookies.jhtml">Cookies info</a></li>




        <li class="nav6"><a     href="../../../../../who-we-are/legal-disclaimer.jhtml">Legal Disclaimer</a></li>




        <li class="nav7"><a     href="../../../../../who-we-are/about-site-provider.jhtml">About Site Provider</a></li>




        <li class="nav8"><a     href="../../../../../who-we-are/provision-of-services.jhtml">Provision of Services</a></li>




        <li class="nav9"><a     href="../../../../../corporate-sustainability/workforce-diversity.jhtml">Diversity</a></li>




        <li class="nav10"><a     href="../../../../../who-we-are/web-accessibility.jhtml">Web Accessibility</a></li>




        <li class="nav11"><a     href="mailto:on-line.presence@uk.pwc.com">Email Webmaster</a></li>




        <li class="nav12"><a     href="https://register.pwc.co.uk/premium/editUserProfile.htm">Edit user details</a></li>

</ul></div>
		</div>
		<!-- Country selector box --> 
		<br clear="all"/> 
		<div id="intllayer"> </div>
		
</div>


      

</body>

</html>