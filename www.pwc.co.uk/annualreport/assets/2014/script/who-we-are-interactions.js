$( document ).ready(function() {

	/* preload images */

	function preload(arrayOfImages) {
    $(arrayOfImages).each(function(){
        $('<img/>')[0].src = this;
        // Alternatively you could use:
        // (new Image()).src = this;
    });
	}

	// Usage:

	preload([
	    '../assets/2014/images/our-strategy/triangle/pyramid-colours-1.png',
	    '../assets/2014/images/our-strategy/triangle/pyramid-colours-2.png',
	    '../assets/2014/images/our-strategy/triangle/pyramid-colours-3.png',
	    '../assets/2014/images/our-strategy/triangle/pyramid-colours-4.png',
	    '../assets/2014/images/our-strategy/triangle/pyramid-colours-5.png',
	    '../assets/2014/images/our-strategy/triangle/pyramid-colours-6.png',
	    '../assets/2014/images/our-strategy/triangle/pyramid-colours-7.png'
	]);

	$(".segment").hover(
		function () {
			if($(this).hasClass('seg1')){
				$('.triangle_container').addClass('seg1-active-temp');
			}
			if($(this).hasClass('seg2')){
				$('.triangle_container').addClass('seg2-active-temp');
			}
			if($(this).hasClass('seg3')){
				$('.triangle_container').addClass('seg3-active-temp');
			}
			if($(this).hasClass('seg4')){
				$('.triangle_container').addClass('seg4-active-temp');
			}
			if($(this).hasClass('seg5')){
				$('.triangle_container').addClass('seg5-active-temp');
			}
			if($(this).hasClass('seg6')){
				$('.triangle_container').addClass('seg6-active-temp');
			}
			if($(this).hasClass('seg7')){
				$('.triangle_container').addClass('seg7-active-temp');
			}
		},
		function () {
			$('.triangle_container').removeClass('seg1-active-temp');
			$('.triangle_container').removeClass('seg2-active-temp');
			$('.triangle_container').removeClass('seg3-active-temp');
			$('.triangle_container').removeClass('seg4-active-temp');
			$('.triangle_container').removeClass('seg5-active-temp');
			$('.triangle_container').removeClass('seg6-active-temp');
			$('.triangle_container').removeClass('seg7-active-temp');
		}
	);

	$( ".segment" ).click(function() {
			
			$('.triangle_container').removeClass('seg1-active');
			$('.triangle_container').removeClass('seg2-active');
			$('.triangle_container').removeClass('seg3-active');
			$('.triangle_container').removeClass('seg4-active');
			$('.triangle_container').removeClass('seg5-active');
			$('.triangle_container').removeClass('seg6-active');
			$('.triangle_container').removeClass('seg7-active');
			$('.desc0').hide();
			$('.desc1').hide();
			$('.desc2').hide();
			$('.desc3').hide();
			$('.desc4').hide();
			$('.desc5').hide();
			$('.desc6').hide();
			$('.desc7').hide();
			
			if($(this).hasClass('seg1')){
				$('.triangle_container').addClass('seg1-active');
				$('.desc1').show();
			}
			if($(this).hasClass('seg2')){
				$('.triangle_container').addClass('seg2-active');
				$('.desc2').show();
			}
			if($(this).hasClass('seg3')){
				$('.triangle_container').addClass('seg3-active');
				$('.desc3').show();
			}
			if($(this).hasClass('seg4')){
				$('.triangle_container').addClass('seg4-active');
				$('.desc4').show();
			}
			if($(this).hasClass('seg5')){
				$('.triangle_container').addClass('seg5-active');
				$('.desc5').show();
			}
			if($(this).hasClass('seg6')){
				$('.triangle_container').addClass('seg6-active');
				$('.desc6').show();
			}
			if($(this).hasClass('seg7')){
				$('.triangle_container').addClass('seg7-active');
				$('.desc7').show();
			}
			
	});

});