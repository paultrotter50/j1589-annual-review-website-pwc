var jsdevice = "mobile";



$(document).ready(function(){

	$(document).keypress(function(event){
	    var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13'){
	        alert('You pressed a "enter" key in somewhere');   
	    }
	});
	
	 if (typeof arr_sidenav != "undefined") {
		for (i=0;i<arr_sidenav.length; i++) {
			if ((window.location.pathname.indexOf(arr_sidenav[i][0]) != -1 )||(window.location.hostname.indexOf(arr_sidenav[i][0]) != -1 )) {
				$("#mobilenav").html(arr_sidenav[i][1]);
			}
		}
	}	
	
	$('#mobileheadermenu').toggle( 
	   function() { $('#wrapper').animate({ right: 200 }, 'fast', function() { }); },  function() { $('#wrapper').animate({ right: 0 }, 'fast', function() { }); }
	);
	$('#mobileheadersearch').toggle( 
	   function() {
		$('#searchfield').toggle();
		$('#searchfield').val("");
		$('#searchfield').focus();
	   }, 
	function() { $('#searchfield').toggle(); }
	);
	$('#searchfield').keyup(function() {
		if ($('#searchfield').val().length > 0) {
			$('#mobileheadersearch').hide();
			$('#mobilesearchsubmit').show();
		} else {
			$('#mobileheadersearch').show();
			$('#mobilesearchsubmit').hide();
		}
	});

	$('#mobilecopyright > div').jTruncate({  
		length: 50,  
	        minTrail: 0,  
	        moreText: "...more",  
	        lessText: ""
    });  
	
	/*
	$("#morecopyright").click(function() {
		$("#mobilecopyright > span").show();
		$(this).hide();
		return false;
	});
	*/
	
	if ($("#feature-mobilehome")) {
		$(window).load(function() { initmobilefeature(); });
		$(window).resize(function() { resizemobilefeature(); });
	}
});


(function(doc) {

    var addEvent = 'addEventListener',
        type = 'gesturestart',
        qsa = 'querySelectorAll',
        scales = [1, 1],
        meta = qsa in doc ? doc[qsa]('meta[name=viewport]') : [];

    function fix() {
        meta.content = 'width=device-width,minimum-scale=' + scales[0] + ',maximum-scale=' + scales[1];
        doc.removeEventListener(type, fix, true);
    }

    if ((meta = meta[meta.length - 1]) && addEvent in doc) {
        fix();
        scales = [.25, 1.6];
        doc[addEvent](type, fix, true);
    }

}(document));

function adjustviewport(vwidth,vscale) {
	$('meta[name=viewport]').attr('content','width=' + vwidth + ', initial-scale=1, minimum-scale=' + vscale + ', maximum-scale=' + vscale);
};

function initmobilefeature() {
	var tempwidth = $(window).width();
	if (tempwidth < 980) {
		$('#feature-mobilehome').css('width', tempwidth);
		$('#feature-mobilehome > .items > div > a > img').css('width', tempwidth);
	}
	$("#feature-mobilehome").scrollable({ circular: true, touch: false }).autoscroll({ autoplay: false, interval: 5000 }).handleSwipes();
	//$("#feature-mobilehome").scrollable({ circular: true, touch: true }).autoscroll({ autoplay: false, interval: 5000 });
}
function resizemobilefeature() {
	var tempwidth = $(window).width();
	if (tempwidth < 980) {
		$('#feature-mobilehome').css('width', tempwidth);
		$('#feature-mobilehome > .items > div > a > img').css('width', tempwidth);
	}
	var api = $("#feature-mobilehome").data("scrollable");
	if(api) { api.prev(); }
}
