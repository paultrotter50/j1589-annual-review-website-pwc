$(function() {
	$('.reqfield').parents('.formfield').append('<span class="formrequired">*</span>');
	$('.reqemail').parents('.formfield').append('<span class="formrequired">*</span>');
/*
	$('.form-dd').mask("99", {placeholder: "d"});
	$('.form-mm').mask("99", {placeholder: "m"});
	$('.form-yy').mask("9999", {placeholder: "y"});
	$('.form-phone').mask("(999) 999 - 9999");
*/
});

$(document).ready(function(){


                // Bind focus for all form fields
                $(':input:not([type="submit"]||[type="reset"]||[id="searchfield"])').focus(function() {
                                 $(this)
                                                  .removeClass("fieldhighlightval2")
                                                  .parent().closest('.formfield').removeClass("fieldhighlightval")
                                                  .addClass("fieldhighlighton")
                                                  .children("label").removeClass("labelval")
                                                  .addClass("labelhighlight")
                                                  .siblings("a").children("img").addClass("fieldhelpon")
                });
                $("select").focus(function() {
                                 $(this)
                                                  .parent().closest('.formfield').addClass("fieldhighlighton")
                                                  .children("label").addClass("labelhighlight")
                                                  .siblings("a").children("img").addClass("fieldhelpon")
                                 });
                $("textarea").focus(function() {
                                 $(this)
                                                  .parent().closest('.formfield').addClass("fieldhighlighton")
                                                  .children("label").addClass("labelhighlight")
                                                  .siblings("a").children("img").addClass("fieldhelpon")
                                 });
                                 
                // Bind blur for all form fields
                $(':input:not([type="submit"]||[type="reset"]||[id="searchfield"])').blur(function() {
                                 $(this)
                                                  .parent().closest('.formfield').removeClass("fieldhighlighton")
                                                  .children("label").removeClass("labelhighlight")
                                                  .siblings("a").children("img").removeClass("fieldhelpon")
                });
                $("select").blur(function() {
                                 $(this)
                                                  .parent().closest('.formfield').removeClass("fieldhighlighton")
                                                  .children("label").removeClass("labelhighlight")
                                                  .siblings("a").children("img").removeClass("fieldhelpon")
                });
                $("textarea").blur(function() {
                                 $(this)
                                                  .parent().closest('.formfield').removeClass("fieldhighlighton")
                                                  .children("label").removeClass("labelhighlight")
                                                  .siblings("a").children("img").removeClass("fieldhelpon")
                });
               
                // Bind events for tooltip
                $("a.fieldhelp").mouseover(function() {
                                 $(this).children(".tooltip").show()
                });
                $("a.fieldhelp").mouseout(function() {
                                 $(this).children(".tooltip").hide()
                });
                $(".fieldhelp").focus(function() {
                                 $(this).children(".tooltip").hide()
                });
              
});



function tooltipoff(item) {
                item.parentNode.getElementsByTagName("span")[1].style.display = "none";
}
function tooltipon(item) {
                item.parentNode.getElementsByTagName("span")[1].style.display = "block";
}
function formreset() { document.location.reload(true); }

function formvalidate() {
       var passvalidation = true;
       
                                 //Check text fields, text areas and select boxes
                                 $("input.reqfield:text, input.reqfield:file, textarea.reqfield, select.reqfield:").each(function(){
                                                  var v = $(this).val();
                                                  if (v == "" || v == " " || v == null){
                                                                   passvalidation = false;
                                                                   var tempthis = $(this);
                                                                   if ((tempthis.parent().parent().attr('class') == "form2column") || (tempthis.parent().parent().attr('class') == "form3column")) { tempthis = $(this).parent().parent(); }
                                                                   tempthis
                                                                                    .addClass("fieldhighlightval2")
                                                                                    .parent().addClass("fieldhighlightval")
                                                                                    .end().siblings("label").addClass("labelval");
                                                  }
       });
       
       // Radio button and checkbox validation
                                 $("input.reqfield:radio, input.reqfield:checkbox").each(function(){
                                                  var thisName = this.name;
                        if( $("input[name="+thisName+"]:checked").length == 0){
                                          passvalidation = false;
                                                                   var tempthis = $(this);
                                                                   if ((tempthis.parent().parent().attr('class') == "form2column") || (tempthis.parent().parent().attr('class') == "form3column")) { tempthis = $(this).parent().parent(); }
                                                                   tempthis
                                                           .removeClass("fieldhighlightval2")
                                                             .parent().addClass("fieldhighlightval")
                                                             .end().siblings("label").addClass("labelval");
                        }else{
                                                                   var tempthis = $(this);
                                                                   if ((tempthis.parent().parent().attr('class') == "form2column") || (tempthis.parent().parent().attr('class') == "form3column")) { tempthis = $(this).parent().parent(); }
                                                                   tempthis
                                                           .removeClass("fieldhighlightval2")
                                                                          .parent().removeClass("fieldhighlightval")
                                                                          .end().siblings("label").removeClass("labelval");
                        }
      });
       
                                 // Email address validation
                                 $("input.reqemail").each(function(){
                                                  if (chkEmail(this.value) == false){
                                                                   passvalidation = false;
                                                                   var tempthis = $(this);
                                                                   if ((tempthis.parent().parent().attr('class') == "form2column") || (tempthis.parent().parent().attr('class') == "form3column")) { tempthis = $(this).parent().parent(); }
                                                                   tempthis
                                                                                    .addClass("fieldhighlightval2")
                                                             .parent().addClass("fieldhighlightval")
                                                             .end().siblings("label").addClass("labelval");
                        }
      });
           
                                 if (passvalidation) {
                                                  return true;
                                 } else {
                                                  alert(formvalidationmsg);
                                                  return false;
                                 }
}

/* Valid email check */
function chkEmail(email) {
 //return(email.indexOf(".") > 1) && (email.indexOf("@") > 0);
 return(email.indexOf("@") > 0);
}


