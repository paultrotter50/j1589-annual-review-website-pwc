// JavaScript Document

var colorArray=["#FCB514","#E98C23","#DABA26","#958B6C","#DF3226","#DB536A","#602320","#968B6C","#A32020"];

var officearray=[{name:'London',category:'greater_london',url:'#london'},{name:'Norwich',category:'south_east',url:'http://pwc.blogs.com/south-east/norwich-office.html'},{name:'Cambridge',category:'south_east',url:'http://pwc.blogs.com/south-east/cambridge-office.html'},{name:'St Albans',category:'south_east',url:'http://pwc.blogs.com/south-east/st-albans-office.html'},
{name:'Uxbridge',category:'south_east',url:'http://pwc.blogs.com/south-east/uxbridge-office.html'},{name:'Reading',category:'south_east',url:'http://pwc.blogs.com/south-east/reading-office.html'},{name:'Gatwick',category:'south_east',url:'http://pwc.blogs.com/south-east/gatwick-office.html'},{name:'Birmingham',category:'midlands',url:'http://pwc.blogs.com/midlands/birmingham-office.html'},{name:'Milton Keynes',category:'midlands',url:'http://pwc.blogs.com/midlands/milton-keynes-office.html'},{name:'East Midlands',category:'midlands',url:'http://pwc.blogs.com/midlands/east-midlands-office.html'},{name:'Newcastle',category:'north_east',url:'http://pwc.blogs.com/north/newcastle-office.html'},{name:'Hull',category:'north_east',url:'http://pwc.blogs.com/north/hull-office.html'},{name:'Leeds',category:'north_east',url:'http://pwc.blogs.com/north/leeds-office.html'},{name:'Sheffield',category:'north_east',url:'http://pwc.blogs.com/north/sheffield-office.html'},{name:'Liverpool',category:'north_west',url:'http://pwc.blogs.com/north/liverpool-office.html'},{name:'Manchester',category:'north_west',url:'http://pwc.blogs.com/north/manchester-office.html'},{name:'Plymouth',category:'west',url:'http://pwc.blogs.com/west/plymouth-office.html'},{name:'Bristol',category:'west',url:'http://pwc.blogs.com/west/bristol-office.html'},{name:'Swansea',category:'wales',url:'http://pwc.blogs.com/wales/swansea-office.html'},{name:'Cardiff',category:'wales',url:'http://pwc.blogs.com/wales/cardiff-office.html'},{name:'Aberdeen',category:'scotland',url:'http://pwc.blogs.com/scotland/aberdeen-office.html'},{name:'Edinburgh',category:'scotland',url:'http://pwc.blogs.com/scotland/edinburgh-office.html'},{name:'Glasgow',category:'scotland',url:'http://pwc.blogs.com/scotland/glasgow-office.html'},{name:'Belfast',category:'northern_ireland',url:'http://pwc.blogs.com/northern-ireland/belfast-office.html'},{name:'Linen Green',category:'northern_ireland',url:'http://pwc.blogs.com/northern-ireland/linen-green-office.html'},{name:'Guernsey',category:'channel_islands',url:'http://www.pwc.com/jg/en/about-us/office-locations.jhtml'},{name:'Jersey',category:'channel_islands',url:'http://www.pwc.com/jg/en/about-us/office-locations.jhtml'}]

var myurl="";


$(document).ready(function(){
	$('.region').each(function(_index){
		var newtitle=$(this).attr('id').split('_').join(' ');
		newtitle=toTitleCase(newtitle);
		$('.map_listholder').append('<div class="map_regiontitle">'+newtitle+'</div>');
		$(this).find('.marker').each(function(){
			newtitle=$(this).attr('id').split('_').join(' ');
			var mynewtitle=toTitleCase(newtitle);
			
			$(this).mouseover(function(){
				showBalloon($(this),mynewtitle);
			})
			$(this).mouseout(function(){
				hideBalloon($(this));
			})
			for(i=0;i<officearray.length;i++){
				if(mynewtitle==officearray[i].name){
					var myurl=officearray[i].url;
					$(this).click(function(){
						window.location=myurl;
					})
				}
			}
			
			$('.map_listholder').append('<div class="map_location" onclick="window.location=\''+testArray($(this).attr('id'))+'\'" onmouseover="$(this).addClass(\'over\');showBalloon($(\'#'+$(this).attr('id')+'\'),\''+mynewtitle+'\');" onmouseout="$(this).removeClass(\'over\');hideBalloon($(\'#'+$(this).attr('id')+'\'))">'+mynewtitle+'</div>');
		})
	})
	if($('.map_listholder').height() < $('#scrollpane').height()){
		$('#scrollbar').css({'display':'none'});
	} else {
		// create fraction
		scrolldistance=($('#scrollpane').height() / $('.map_listholder').height());
		thumbdistance=($('.map_listholder').height() / $('#scrollpane').height());
		$('#scrollthumb').css({'height':(360*scrolldistance)+'px'});
		$('#scrollthumb').addTouch();
		$( "#scrollthumb" ).draggable({ containment: 'parent',drag:function(){
			$('.map_listholder').css({'top':-($('#scrollthumb').position().top*thumbdistance)+'px'});
		}});
		$('.map_listholder').draggable({'axis':'y',containment:[0,30+($(this).parent().height-$(this).height())*-1,0,30],drag:function(){
			$('#scrollthumb').css({'top':-($(this).position().top*scrolldistance)+'px'});
		}});
	}
})
function showBalloon(_item,_str){
	for(i=0;i<officearray.length;i++){
				if(officearray[i].name==_str){
					myurl=officearray[i].url;
				}
			}
	//_item.attr({'fill':'#000'});
	$(_item).css({'background-color':'#000'});
	$('#balloon').css({'display':'block'});
	//$('#balloon').attr({'transform':'translate('+(_item.attr('x')-60)+','+(_item.attr('y')-58)+')'});
	$('#balloon').css({"left":$(_item).position().left-50+'px',"top":$(_item).position().top-58+'px'});
	//alert('item: '+Number(_item.attr('x')));
	$('#balloontext').html(_str);
}

function testArray(name){
	name=name.split('_').join(' ');
	for(i=0;i<officearray.length;i++){
		if(officearray[i].name==toTitleCase(name)){
			return officearray[i].url;
		}
	}
}

function toTitleCase(str){
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function hideBalloon(_item){
	$('#balloon').css({'display':'none'});
	$(_item).css({'background-color':'#FFF'});
}

function oc(a){
  var o = {};
  for(var i=0;i<a.length;i++)
  {
    o[a[i]]='';
  }
  return o;
}