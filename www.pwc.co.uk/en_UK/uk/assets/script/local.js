/*** AJAX & JS Configuration parameters for territories ***/

/* Live search */

	var livesearchon = ""; //Enable or disable live search. "true" for on. blank or any other value for off.
	var minchars = 2; //Min number of characters input before suggestions are triggered

	/* Careers livesearch on but not main site */
	var url = location.pathname;
	if (url.indexOf('/careers') > -1) {
		livesearchon = "true";
	}

	// Change to location of custom live search xml file
	var suggestionsfile = "/en_GX/webadmin/ajax/livesearch/livesearch.xml";
	//var suggestionsfile = "http://www.pwc.com/en_GX/webadmin/esi/liveresults.xml?pwcGeo=CA&pwcLang=en";

/* Send & share links */

	// NB! This is required for all cc-ll.js files regardless
	var arr_ss = new Array();

// Localised mobile sidenav(s). First attribute is folder & second is sidenav HTML
var arr_sidenav = new Array();

arr_sidenav [0] = new Array()
arr_sidenav [0][0] = "/website/www_pwclegal_co_uk/"; // Folder
arr_sidenav [0][1] = "<ul><li><a href='/website/www_pwclegal_co_uk/index.jhtml'>Home</a></li><li><a href='/website/www_pwclegal_co_uk/services/index.jhtml'>Services</a></li><li><a href='/website/www_pwclegal_co_uk/issues/index.jhtml'>Issues</a></li><li><a href='/website/www_pwclegal_co_uk/our-people/index.jhtml'>Our people</a></li><li><a href='/website/www_pwclegal_co_uk/who-we-are.jhtml'>Who we are</a></li><li><a href='http://www.pwc.co.uk/careers/student/graduateopportunities/pwc-legal.jhtml'>Careers</a></li></ul>";

arr_sidenav [1] = new Array()
arr_sidenav [1][0] = "www.pwclegal.co.uk"; // Domain
arr_sidenav [1][1] = "<ul><li><a href='/website/www_pwclegal_co_uk/index.jhtml'>Home</a></li><li><a href='/website/www_pwclegal_co_uk/services/index.jhtml'>Services</a></li><li><a href='/website/www_pwclegal_co_uk/issues/index.jhtml'>Issues</a></li><li><a href='/website/www_pwclegal_co_uk/our-people/index.jhtml'>Our people</a></li><li><a href='/website/www_pwclegal_co_uk/who-we-are.jhtml'>Who we are</a></li><li><a href='http://www.pwc.co.uk/careers/student/graduateopportunities/pwc-legal.jhtml'>Careers</a></li></ul>";

	arr_sidenav [2] = new Array()
	arr_sidenav [2][0] = "/careers/schools/";
	arr_sidenav [2][1] = "<ul><li><a href='/uk/en/careers/index.jhtml'>Home</a></li><li><a href='/uk/en/careers/schools/aboutpwc/about-pwc.jhtml'>About PwC</a></li><li><a href='/uk/en/careers/schools/ourevents/events.jhtml'>Our events</a></li><li><a href='/uk/en/careers/schools/careers/our-opportunities.jhtml'>Career opportunities</a></li><li><a href='https://app.work4labs.com/mobile-job-search?page_id=265250856828165'>Search all our jobs</a></li><li><a href='/uk/en/careers/schools/applying/applying.jhtml'>Applying</a></li><li><a href='/uk/en/careers/schools/yourdevelopment/your-development.jhtml'>Your development</a></li><li><a href='/uk/en/careers/schools/payandbenefits/pay-benefits.jhtml'>Pay and benefits</a></li><li><a href='/uk/en/careers/schools/ptzone/parent-teacher-zone.jhtml'>Parent & teacher zone</a></li><li><a href='/uk/en/careers/schools/faqs/faqs.jhtml'>FAQs</a></li></ul>";

	arr_sidenav [3] = new Array()
	arr_sidenav [3][0] = "/careers/student/";
	arr_sidenav [3][1] = "<ul><li><a href='/uk/en/careers/student/index.jhtml'>Homepage</a></li><li><a href='/uk/en/careers/student/aboutpwc/about-pwc.jhtml'>About PwC</a></li><li><a href='/uk/en/careers/student/workexperience/work-experience.jhtml'>Work experience</a></li><li><a href='/uk/en/careers/student/graduateopportunities/our-opportunities.jhtml'>Graduate opportunities</a></li><li><a href='https://app.work4labs.com/mobile-job-search?page_id=265250856828165'>Search jobs</a></li><li><a href='/uk/en/careers/student/traininglearningdevelopment/training-development.jhtml'>Training & development </a></li><li><a href='/uk/en/careers/student/payandbenefits/pay-benefits.jhtml'>Pay & benefits</a></li><li><a href='http://www.youtube.com/user/careerspwc'>Our videos</a></li><li><a href='/uk/en/careers/student/ourevents/find-out-more-events.jhtml'>Our events</a></li><li><a href='/uk/en/careers/student/applying/applying.jhtml'>Applying</a></li><li><a href='/uk/en/careers/student/faqs/frequently-asked-questions-home.jhtml'>FAQs</a></li></ul>";

	arr_sidenav [4] = new Array()
	arr_sidenav [4][0] = "/careers/experienced/";
	arr_sidenav [4][1] = "<ul><li><a href='/uk/en/careers/experienced/index.jhtml'>Homepage</a></li><li><a href='/uk/en/careers/experienced/who-are-we.jhtml'>Who we are</a></li><li><a href='/uk/en/careers/experienced/actuarial.jhtml'>Actuarial</a></li><li><a href='/uk/en/careers/experienced/assurance.jhtml'>Assurance</a></li><li><a href='/uk/en/careers/experienced/consulting.jhtml'>Consulting</a></li><li><a href='/uk/en/careers/experienced/tax.jhtml'>Tax</a></li><li><a href='/uk/en/careers/experienced/benefits.jhtml'>Benefits</a></li><li><a href='/uk/en/careers/experienced/work-life-balance.jhtml'>Work/life balance</a></li><li><a href='/uk/en/careers/experienced/recruitment-agencies.jhtml'>Agencies working with PwC</a></li><li><a href='/uk/en/careers/experienced/applying-to-pwc.jhtml'>Applying to PwC</a></li></ul>";

arr_sidenav [5] = new Array()
arr_sidenav [5][0] = "/annualreport/"; // Folder
arr_sidenav [5][1] = "<ul><li><a href='/annualreport/index.jhtml'>Home</a></li><li><a href='/uk/en/annualreport/our-strategy/index.jhtml'>Our strategy</a></li><li><a href='/uk/en/annualreport/business-review/index.jhtml'>Business review</a></li><li><a href='/uk/en/annualreport/reputation-risk-quality/index.jhtml'>Reputation, quality and risk</a></li><li><a href='/uk/en/annualreport/governance/executive-board.jhtml'>Governance</a></li><li><a href='/uk/en/annualreport/financial-performance/index.jhtml'>Our financial performance</a></li><li><a href='/annualreport/global-network/index.jhtml'>Global network</a></li><li><a href='/annualreport/case-studies/index.jhtml'>Case studies</a></li></ul>";

/* Placeholder */
/*
$('.uk-linkedin > a').attr('href', $(this).attr('href')+"marktest");
*/

$(document).ready(function(){
	setTimeout(function(){
		//alert($('.uk-linkedin > a').attr('href'));
	},3000);
});

