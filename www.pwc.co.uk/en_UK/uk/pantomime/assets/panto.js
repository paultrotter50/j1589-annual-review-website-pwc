// JavaScript Document
var selected=0;
var timeout;
var interval=setInterval(advanceImg,4000);
$('#img_0').css({'border-bottom':'1px solid #FFF'});


$(document).ready(function(){
	init();
})

function init(){
	$('#thumbbar').mouseenter(function(){
		$(this).animate({'opacity':'1'},250);
	})
	$('#thumbbar').mouseleave(function(){
		$(this).animate({'opacity':'0.3'},250);
	})
	$('#thumbbar > img').mouseenter(function(){
		$(this).css({'border-bottom':'1px solid #FFF'});
	})
	$('#thumbbar > img').mouseleave(function(){
		if($(this).attr('id').split('_')[1]!=selected){
			$(this).css({'border-bottom':'none'});
		}
	})
	$('#thumbbar > img').click(function(){
		clearInterval(interval);
		//clearTimeout
		var mynum=$(this).attr('id').split('_')[1];
		jump(mynum);
	})
}

function advanceImg(){
	$('#thumb_'+selected).css({'border-bottom':'none'});
	if(selected<$('#images > img').length-1){
		selected++;
	} else {
		selected=0;
	}
	$('#images').animate({'left':selected*-400},250);
	$('#thumb_'+selected).css({'border-bottom':'1px solid #FFF'});
}

function jump(_num){
	$('#thumb_'+selected).css({'border-bottom':'none'});
	selected=_num;
	$('#thumb_'+selected).css({'border-bottom':'1px solid #FFF'});
	$('#images').animate({'left':selected*-400},250);
	timeout=setTimeout(resetInterval,8000);
}

function resetInterval(){
	try{
		clearTimeout(timeout);
	} catch(e){
		// nothing
	}
	interval=setInterval(advanceImg,4000);
}